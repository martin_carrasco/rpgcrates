package io.github.martin_carrasco.RPGcrates;

public enum CrateType
{
    TYPE1("koth"),
    TYPE2("vote"),
    TYPE3("reward"),
    TYPE4("common");
    
    private final String name;
    
    CrateType(String name)
    {
        this.name = name;
    }
    
    public String getName(){return name;}
    public static CrateType nameToCrate(String name) throws Exception
    {
        switch(name.toLowerCase())
        {
            case "koth":
                return TYPE1;
            case "vote":
                return TYPE2;
            case "reward":
                return TYPE3;
            case "common":
                return TYPE4;
            default:
                throw new Exception("Crate type not found!");
        }
    }
}
