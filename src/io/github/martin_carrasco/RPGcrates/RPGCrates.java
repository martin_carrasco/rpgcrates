package io.github.martin_carrasco.RPGcrates;

import org.bukkit.plugin.java.JavaPlugin;
/**
 *
 * @author Martin Carrasco
 */
public class RPGCrates extends JavaPlugin
{
    //public static Economy econ;
    private static RPGCrates instance;
    @Override
    public void onEnable()
    {
        //if(!setupEconomy())
        //{
            //this.getPluginLoader().disablePlugin(this);
        //}
        //econ = this.getServer().getServicesManager().getRegistration(Economy.class).getProvider();
        this.saveDefaultConfig();
        instance = this;
        this.getServer().getPluginManager().registerEvents(new cListener(), this);
        this.getCommand("crates").setExecutor(new CMD_crate());
    }
    public static RPGCrates getInstance(){return instance;}
    /*private boolean setupEconomy() 
    {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }*/
}
