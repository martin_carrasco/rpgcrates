package io.github.martin_carrasco.RPGcrates;

import java.lang.reflect.Field;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.TileEntity;
import net.minecraft.server.v1_8_R3.TileEntityChest;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.block.CraftChest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CMD_crate implements CommandExecutor
{

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String string, String[] args) 
    {
        if(cmd.getName().equalsIgnoreCase("crates"))
        {
            RPGCrates plugin = RPGCrates.getInstance();
            if(args.length == 3)
            {
                if(args[0].equalsIgnoreCase("add"))
                {
                    try {
                        if(!cs.hasPermission("rpgcrates.add")){cs.sendMessage(ChatColor.RED + "You don't have permission to do this!");return true;}
                        CrateType type = CrateType.nameToCrate(args[1]);
                        Player p = plugin.getServer().getPlayer(args[2]);
                        if(p != null)
                        {
                            ItemStack i = new ItemStack(Material.TRIPWIRE_HOOK);
                            ItemMeta im = i.getItemMeta();
                            im.setDisplayName(ChatColor.RED + type.getName());
                            i.setItemMeta(im);
                            p.getInventory().addItem(i);
                            p.sendMessage(ChatColor.GREEN + "You got a " + type.getName() + " crate key!");
                            cs.sendMessage(ChatColor.GREEN + "Player got their key!");
                        }
                        return true;
                    } catch (Exception ex) {
                        Logger.getLogger(CMD_crate.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            if(args.length == 2)
            {
                if(args[0].equalsIgnoreCase("create") && cs instanceof Player)
                {
                    if(cs.hasPermission("rpgcrates.create"))
                    {
                        try
                        {
                            Player p = (Player) cs;
                            CrateType ct = CrateType.nameToCrate(args[1]);
                            p.getLocation().getDirection();
                            org.bukkit.util.Vector dir = p.getLocation().getDirection();
                            org.bukkit.util.Vector v = new org.bukkit.util.Vector(1, 1, 0);
                            dir.add(v);
                            Block b = p.getWorld().getBlockAt(dir.toLocation(p.getWorld()));
                            setName(b.getLocation(), ct.getName());
                            p.sendMessage(ChatColor.GREEN + "Chest created!");
                        }
                    catch(Exception ex){cs.sendMessage("Error " + ex.getMessage() + " " + ex.getLocalizedMessage());}
                    }
                    else
                    {
                        cs.sendMessage(ChatColor.RED+ "You don't have permission");
                    }
                }
            }
        }
        return false;
    }
    public static void setName(Location loc, String name)
    {
        try
        {
            loc.getBlock().setType(Material.CHEST);
            Field inventoryField = CraftChest.class.getDeclaredField("chest");
            inventoryField.setAccessible(true);
            TileEntityChest teChest = ((TileEntityChest) inventoryField.get((CraftChest)loc.getBlock().getState()));
            teChest.a(name);
        }
        catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e)
        {
        }
    }
    
}
