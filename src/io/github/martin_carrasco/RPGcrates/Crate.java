package io.github.martin_carrasco.RPGcrates;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

public class Crate 
{
    private CrateType type;
    private List<Integer> list;
    //private Location loc;
    Crate(CrateType type) throws Exception
    {
        this.type = type;
        RPGCrates plugin = RPGCrates.getInstance();
        if(plugin.getConfig().getConfigurationSection("Crates").getKeys(false) == null)
        {   
            throw new Exception("This crate type doesn't exist");
        }
        list = plugin.getConfig().getIntegerList("Crates." + type.getName() + ".items");
        /*this.loc = new Location(plugin.getServer().getWorld(plugin.getConfig().getString("Crates." + type.getName() + ".loc.world"))
                                , plugin.getConfig().getInt("Crates." + type.getName() + ".loc.x")
                                , plugin.getConfig().getInt("Crates." + type.getName() + ".loc.y")
                                , plugin.getConfig().getInt("Crates." + type.getName() + ".loc.z"));*/
    }
    
    public CrateType getType(){return type;}
    public List<Integer> getItems(){return list;}
   // public Location getLocation(){return loc;}
}
