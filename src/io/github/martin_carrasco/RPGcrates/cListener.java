package io.github.martin_carrasco.RPGcrates;

import java.util.Random;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class cListener implements Listener
{
    @EventHandler
    public void onPunch(PlayerInteractEvent e)
    {
        if(e.getPlayer().getItemInHand().getType() == Material.TRIPWIRE_HOOK
            && (ChatColor.stripColor(e.getPlayer().getItemInHand().getItemMeta().getDisplayName()).equals(CrateType.TYPE1.getName())
                || ChatColor.stripColor(e.getPlayer().getItemInHand().getItemMeta().getDisplayName()).equals(CrateType.TYPE2.getName())
                || ChatColor.stripColor(e.getPlayer().getItemInHand().getItemMeta().getDisplayName()).equals(CrateType.TYPE3.getName())
                || ChatColor.stripColor(e.getPlayer().getItemInHand().getItemMeta().getDisplayName()).equals(CrateType.TYPE4.getName()))
            && e.getClickedBlock().getType() == Material.CHEST
            && e.getAction() == Action.RIGHT_CLICK_BLOCK)
        {
            
            try
            {
                CrateType type = CrateType.nameToCrate(((Chest)e.getClickedBlock().getState()).getInventory().getName()); //Throws exception if chest isn't a crate
                e.setCancelled(true);
                Crate c = new Crate(type);
                if(c.getType().getName().equalsIgnoreCase(ChatColor.stripColor(e.getPlayer().getItemInHand().getItemMeta().getDisplayName())))
                {
                    int num = e.getPlayer().getItemInHand().getAmount();
                    if(num > 1){e.getPlayer().getItemInHand().setAmount(num - 1);}
                    if(num == 1){e.getPlayer().getInventory().remove(e.getPlayer().getItemInHand());}
                    Random r = new Random();
                    int it = c.getItems().get(r.nextInt(c.getItems().size()));
                    if(it == 555)
                    {
                        int amount = 5000;
                        //RPGCrates.econ.depositPlayer(e.getPlayer(), amount);
                        //e.getPlayer().sendMessage(ChatColor.GREEN + "You received  " + RPGCrates.econ.format(amount));
                        return;
                    }
                    if(it == 556)
                    {
                        int amount = 10000;
                        //RPGCrates.econ.depositPlayer(e.getPlayer(), amount);
                        //e.getPlayer().sendMessage(ChatColor.GREEN + "You received  " + RPGCrates.econ.format(amount));
                        return;
                    }
                    if(it == 557)
                    {
                        int amount = 25000;
                        //RPGCrates.econ.depositPlayer(e.getPlayer(), amount);
                        //e.getPlayer().sendMessage(ChatColor.GREEN + "You received  " + RPGCrates.econ.format(amount));
                        return;
                    }
                    if(it == 558)
                    {
                        int amount = 50000;
                        //RPGCrates.econ.depositPlayer(e.getPlayer(), amount);
                        //e.getPlayer().sendMessage(ChatColor.GREEN + "You received  " + RPGCrates.econ.format(amount));
                        return;
                    }
                    if(it == 559)
                    {
                        int amount = 75000;
                        //RPGCrates.econ.depositPlayer(e.getPlayer(), amount);
                        //e.getPlayer().sendMessage(ChatColor.GREEN + "You received  " + RPGCrates.econ.format(amount));
                        return;
                    }
                    if(it == 560)
                    {
                        int amount = 100000;
                       // RPGCrates.econ.depositPlayer(e.getPlayer(), amount);
                        //e.getPlayer().sendMessage(ChatColor.GREEN + "You received  " + RPGCrates.econ.format(amount));
                        return;
                    }
                    if(it == 561)
                    {
                        int amount = 250000;
                        //RPGCrates.econ.depositPlayer(e.getPlayer(), amount);
                        //e.getPlayer().sendMessage(ChatColor.GREEN + "You received  " + RPGCrates.econ.format(amount));
                        return;
                    }
                    e.getPlayer().getInventory().addItem(new ItemStack(it));
                    e.getPlayer().sendMessage(ChatColor.GREEN + "You received a " + new ItemStack(it).getType().name());
                }
                else
                {
                    e.getPlayer().sendMessage("Chest no has name" );
                }
            }
            catch(Exception ex){e.getPlayer().sendMessage(ex.getLocalizedMessage() + "Error");}
            
        }
    }
}
